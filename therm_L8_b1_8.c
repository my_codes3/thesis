
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <string.h>
#include <time.h>
#include <gsl/gsl_rng.h>
//#include <gsl_rng.h>
// lf gsl_rng_uniform(r) to obtain a double. To obtain int: multiply by 2^42 and then take floor


typedef struct listg{
	int site[4];
	int var;
	struct listg* next;
	struct listg* prev;
} listg_t;

int L=8;
int N;
int V;
double beta=1;
double acc=0;
double makeint= pow(2.0, 42);
gsl_rng *r;
const gsl_rng_type *T;

double action(double *p[L][L][L][L]);
double metropolis(double *p[L][L][L][L], double E, double beta);
int gauge(double *p[L][L][L][L]);
int initialize(double *p[L][L][L][L]);



int main(){
	printf("%lf \n", makeint);
	//srand(7);
	gsl_rng_env_setup();
	T= gsl_rng_ranlxd2;
	r=gsl_rng_alloc(T);
	gsl_rng_set(r, 579238310);
	FILE *itp, *itpW1real, *itpacc, *itpdE, *itpE, *itpraw1;
	double M[L][L][L][L][4], *p[L][L][L][L], A, exp1, e[8];
	clock_t start, t1;
	double time_used;
	long double devE;
	int Nsweep=0, Nsubsweep=100, m, index, Nthermalization=100000000, Nstored;  //provisional, just to recheck the code
	unsigned long long i, j, k, l, mi;
	double E[1000100], Etmp, W, varW1=0;    //for the actual code
	//double E[Nthermalization];		//to find tau_nl
	
	double _Complex avgW1=0, W1[Nsweep];
	//printf("ok\n");
	fflush(stdout);
	char title1[100]="wilson1_L8_beta1_e", num[50], title2[100], title3[100]="raw_wilson1_L8_beta1_e", title4[100], title5[100]="raw_energy_L8_beta1_bis.txt", title6[100]="lattice_L8_beta1_";
	N=pow(L,4);
	V=pow(L,3); 
	start=clock();
	for (i=0; i<8; i++){
		e[i]=(4-0.0078)*i/7 +0.0078;
	}
	for (index=0; index<1; index++){
		strncpy(title2, title1, sizeof(title2));
		sprintf(num, "%f", e[index]);
		strcat(title2, num);
		strcat(title2, "_bis.txt");
		strncpy(title4, title3, sizeof(title4));
		strcat(title4, num);
		strcat(title4, "_bis.txt");
		strcat(title6, num);	
		strcat(title6, "_bis.txt");
		for (i=0; i<L; i++){
			for (j=0; j<L; j++){
				for (k=0; k<L; k++){
					for (l=0; l<L; l++){
						p[i][j][k][l]=&M[i][j][k][l][0];
					};
				};
			};
		};

		itp=fopen("./lattice_L8_beta1_8.txt", "r");
		for (i=0; i<L; i++){
			for (j=0; j<L; j++){
				for (k=0; k<L; k++){
					for (l=0; l<L; l++){
						for (mi=0; mi<4; mi++){
							fscanf(itp, "%lf", (p[i][j][k][l]+mi));
						}
					}
				}
			}
		}
		
		fclose(itp);
		
		//initialize(p);
		gauge(p);
		Etmp=action(p);		
		j=0;
		printf("here ok\n");
		for (i=0; i<Nthermalization; i++){
			Etmp=metropolis(p,Etmp,beta);
			if (i%100 ==0){
				E[j]=Etmp;
				j++;
				t1=clock();
				time_used= ((double) (t1-start))/CLOCKS_PER_SEC;
				time_used /= 60;	//I get the minutes
				if (time_used > 1410){ 	//almost 24hours, suited for piz-daint normal computations
					break;
				}
			}
		};
		Nstored=j;
		//printf("end therm\n");
		Etmp=action(p);
		
		
		if (index==0){
			itp=fopen("therm_b1_9.txt", "w");
			for (i=0; i<Nstored; i++){
				fprintf(itp, "%.12f\n", E[i]);
			}
			fclose(itp);
		}
		printf("here ok\n");
		itp=fopen("lattice_L8_beta1_9.txt", "w");
		for (i=0; i<L; i++){
			for (j=0; j<L; j++){
				for (k=0; k<L; k++){
					for (l=0; l<L; l++){
						for (mi=0; mi<4; mi++){
							fprintf(itp, "%.21f\n", *(p[i][j][k][l]+mi));
						}
					}
				}
			}
		}
		fclose(itp);
	}
	
	return 0;
}

int gauge(double *p[L][L][L][L]){
	int ptmp[L][L][L][L], i, j ,k ,l, a;
	listg_t *head, *tmp, *tmp1;
	for (i=0; i<L; i++){
		for (j=0; j<L; j++){
			for (k=0; k<L; k++){
				for (l=0; l<L; l++){
					ptmp[i][j][k][l]=0;
				};
			};
		};
	};
	ptmp[0][0][0][0]=1;
	head=malloc(sizeof(listg_t));
	tmp=head;
	tmp1=head;
	tmp->site[0]=0;
	tmp->site[1]=0;
	tmp->site[2]=0;
	tmp->site[3]=0;
	tmp->var=0;
	tmp->prev=NULL;
	for (a=1; a<N; a++){
		tmp->next=malloc(sizeof(listg_t));
		i=tmp->site[0];
		j=tmp->site[1];
		k=tmp->site[2];
		l=tmp->site[3];
		tmp=tmp->next;
		tmp->next=NULL;
		tmp->site[0]=i;
		tmp->site[1]=j;
		tmp->site[2]=k;
		tmp->site[3]=l;
		tmp->prev=tmp1;
		tmp1=tmp;
		if (ptmp[(i+1)%L][j][k][l]==0){
			ptmp[(i+1)%L][j][k][l]=1;
			tmp->site[0]=(i+1)%L;
			tmp->var=0;
		}
		else if (ptmp[i][(j+1)%L][k][l]==0){
			ptmp[i][(j+1)%L][k][l]=1;
			tmp->site[1]=(j+1)%L;
			tmp->var=1;
		}
		else if (ptmp[i][j][(k+1)%L][l]==0){
			ptmp[i][j][(k+1)%L][l]=1;
			tmp->site[2]=(k+1)%L;
			tmp->var=2;
		}
		else if (ptmp[i][j][k][(l+1)%L]==0){
			ptmp[i][j][k][(l+1)%L]=1;
			tmp->site[3]=(l+1)%L;
			tmp->var=3;
		}
		else {
			
		};
	};
		
		
	while(tmp1!=NULL){
		i=tmp->site[0];
		j=tmp->site[1];
		k=tmp->site[2];
		l=tmp->site[3];
		tmp1=tmp->prev;
		switch (tmp->var){
			case 0: 
				*(p[i][j][k][l])=*(p[(i+1)%L][j][k][l])+*(p[i][(j+1)%L][k][l]+1)+*(p[i][j][(k+1)%L][l]+2)+*(p[i][j][k][(l+1)%L]+3)-*(p[i][j][k][l]+1)-*(p[i][j][k][l]+2)-*(p[i][j][k][l]+3);
			break;
			
			case 1:
				*(p[i][j][k][l]+1)=*(p[(i+1)%L][j][k][l])+*(p[i][(j+1)%L][k][l]+1)+*(p[i][j][(k+1)%L][l]+2)+*(p[i][j][k][(l+1)%L]+3)-*(p[i][j][k][l])-*(p[i][j][k][l]+2)-*(p[i][j][k][l]+3);
			break;
			
			case 2:
				*(p[i][j][k][l]+2)=*(p[(i+1)%L][j][k][l])+*(p[i][(j+1)%L][k][l]+1)+*(p[i][j][(k+1)%L][l]+2)+*(p[i][j][k][(l+1)%L]+3)-*(p[i][j][k][l])-*(p[i][j][k][l]+1)-*(p[i][j][k][l]+3);
			break;
			
			case 3:
				*(p[i][j][k][l]+3)=*(p[(i+1)%L][j][k][l])+*(p[i][(j+1)%L][k][l]+1)+*(p[i][j][(k+1)%L][l]+2)+*(p[i][j][k][(l+1)%L]+3)-*(p[i][j][k][l])-*(p[i][j][k][l]+1)-*(p[i][j][k][l]+2);
			break;
		};
		free(tmp);
		tmp=tmp1;
				
	};
	free(tmp);
	return 0;
}





double metropolis(double *p[L][L][L][L], double E, double beta){
	unsigned long long i, j, k, l, mi;
	double E_new, M[L][L][L][L][4], *ptmp[L][L][L][L], dA, q, expon;
	//printf("E1=%lf\n",E);
	for (i=0; i<L; i++){
		for (j=0; j<L; j++){
			for (k=0; k<L; k++){
				for (l=0; l<L; l++){
					ptmp[i][j][k][l]=&M[i][j][k][l][0];
					for (mi=0; mi<4; mi++){
						*(ptmp[i][j][k][l]+mi) = *(p[i][j][k][l]+mi);
					};
				};
			};
		};
	};
	i=gsl_rng_get(r)%L;
	j=gsl_rng_get(r)%L;
	k=gsl_rng_get(r)%L;
	l=gsl_rng_get(r)%L;
	mi=gsl_rng_get(r)%4;
	if (gsl_rng_uniform(r)>0.5){
		dA=gsl_rng_uniform(r);		
	}
	else{
		dA=-gsl_rng_uniform(r);
	}
	//printf("i=%llu, dA=%lf\n", i, dA);
	//dA /= 2.3; the one which thermalizes (?)
	dA /= 100;
	*(ptmp[i][j][k][l]+mi) += dA;
	gauge(ptmp);
	//printf("E2=%lf\n",E);
	E_new=action(ptmp);
	//printf("E_new=%lf\n", E_new);
	if (E_new==E){
		//printf("E3=%lf\n",E);
		return metropolis(p, E, beta);
		
	};
	expon=exp(E-E_new);
	if (E_new<=E){
		acc++;
		for (i=0; i<L; i++){
			for (j=0; j<L; j++){
				for (k=0; k<L; k++){
					for (l=0; l<L; l++){
						for (mi=0; mi<4; mi++){
							*(p[i][j][k][l]+mi)=*(ptmp[i][j][k][l]+mi);
						};
					};
				};
			};
		};
		//printf("return E_new=%lf\n", E_new);
		return E_new;
	}
	
	else{
		q=gsl_rng_uniform(r);
		if (q<exp(E-E_new)){
			acc++;
			for (i=0; i<L; i++){
				for (j=0; j<L; j++){
					for (k=0; k<L; k++){
						for (l=0; l<L; l++){
							for (mi=0; mi<4; mi++){
								*(p[i][j][k][l]+mi)=*(ptmp[i][j][k][l]+mi);
							};
						};
					};
				};
			};
			//printf("return E_new=%lf\n", E_new);
			return E_new;
		};
		//printf("return E=%lf\n", E);
		//printf("E4=%lf\n",E);
		return E;
	};
	//printf("return E=%lf\n", E);
	//printf("E5=%lf\n",E);
	return E;
}



double action(double *p[L][L][L][L]){
	double S=0, f1, f2;
	int i, j, k, l, mi, ni, itmp1, jtmp1, ktmp1, ltmp1, itmp2, jtmp2, ktmp2, ltmp2, a, midual, nidual, itmp3, jtmp3, ktmp3, ltmp3, itmp4, jtmp4, ktmp4, ltmp4, c;
	for (i=0; i<L; i++){
		for (j=0; j<L; j++){ 
			for (k=0; k<L; k++){
				for (l=0; l<L; l++){
					f1=0;
					f2=0;
					for (mi=1; mi<4; mi++){
						if (mi==1){
							itmp1=i;
							jtmp1=(j+1)%L;
							ktmp1=k;
							ltmp1=l;
						};
						if (mi==2){
							itmp1=i;
							jtmp1=j;
							ktmp1=(k+1)%L;
							ltmp1=l;
						};
						if (mi==3){
							itmp1=i;
							jtmp1=j;
							ktmp1=k;
							ltmp1=(l+1)%L;
						};
						for (ni=0; ni<mi; ni++){
							if (ni==0){
								itmp2=(i+1)%L;
								jtmp2=j;
								ktmp2=k;
								ltmp2=l;
							};
							if (ni==1){
								itmp2=i;
								jtmp2=(j+1)%L;
								ktmp2=k;
								ltmp2=l;
							};
							if (ni==2){
								itmp2=i;
								jtmp2=j;
								ktmp2=(k+1)%L;
								ltmp2=l;
							};
							
							f1 += pow((*(p[itmp1][jtmp1][ktmp1][ltmp1]+ni) - *(p[i][j][k][l] + ni) + *(p[i][j][k][l] + mi) - *(p[itmp2][jtmp2][ktmp2][ltmp2]+mi)),2);
							//printf("f1=%lf \n", f1);
							
							//f1 = f1/4;

// Here I am computing (F_mi_ni)^2=(A_ni(x+mi)-A_ni(x)+A_mi(x)-A_mi(x+ni))^2      where the "vector" tmp1=x+mi and tmp2=x+ni 
							
							midual=-1;
							for (a=0; a<4; a++){
								if (mi != a && ni != a){
									if (midual == -1){
										midual= a;
										if (midual==0){
											itmp3=(i+1)%L;
											jtmp3=j;
											ktmp3=k;
											ltmp3=l;
										};
										if (midual==1){
											itmp3=i;
											jtmp3=(j+1)%L;
											ktmp3=k;
											ltmp3=l;
										};
										if (midual==2){
											itmp3=i;
											jtmp3=j;
											ktmp3=(k+1)%L;
											ltmp3=l;
										};
										if (midual==3){
											itmp3=i;
											jtmp3=j;
											ktmp3=k;
											ltmp3=(l+1)%L;
										};
									}
									else{
										nidual= a;
										if (nidual==0){
											itmp4=(i+1)%L;
											jtmp4=j;
											ktmp4=k;
											ltmp4=l;
										};
										if (nidual==1){
											itmp4=i;
											jtmp4=(j+1)%L;
											ktmp4=k;
											ltmp4=l;
										};
										if (nidual==2){
											itmp4=i;
											jtmp4=j;
											ktmp4=(k+1)%L;
											ltmp4=l;
										};
										if (nidual==3){
											itmp4=i;
											jtmp4=j;
											ktmp4=k;
											ltmp4=(l+1)%L;
										};
									};
								};
							};
							c=-1;
							if (nidual==2 && midual==0){
								c=1;
							}
							if (nidual==3 && midual==1){
								c=1;
							}
							f2 +=  c*(*(p[itmp1][jtmp1][ktmp1][ltmp1]+ni) - *(p[i][j][k][l] + ni) + *(p[i][j][k][l] + mi) - *(p[itmp2][jtmp2][ktmp2][ltmp2]+mi))*(*(p[itmp3][jtmp3][ktmp3][ltmp3]+nidual) - *(p[i][j][k][l] + nidual) + *(p[i][j][k][l] + midual) - *(p[itmp4][jtmp4][ktmp4][ltmp4]+midual));
			//This is F_mi_ni* dual(F_mi_ni)
							//printf("f2=%lf\n", f2);
							
							
						};						
					};
					
					f2 = pow(f2, 2)/4;
					//f1 = 2*f1;	
					/*if(i==(L-2)){
						printf("f2=%lf \n", f2);
						printf("f1=%lf \n", f1);
					}*/
					S += (sqrt(1+ f1/beta + f2/(pow(beta, 2))) -1);
					//printf("S=%lf\n", S);
					//S += (sqrt(1+f1/beta) -1);
					
					/*if(i==(L-2) && j==4 && k==2 && l==(L-1)){
						printf("S partial before= %lf \n", S);
					}*/
					/*if(i==(L-2)){
						printf("S partial after= %lf \n", S);
					}*/
				};
			};
		};
	};
	//printf("S*beta= %lf\n", beta*S);
	return beta*S;
}

int initialize(double *p[L][L][L][L]){
	/*  this function initializes a lattice of size L 
		with Landau gauge choice, sum(A_j(x+j))-A_j)=0*/
	int i,j,mi,k,l;
	//double gaugev;
	for (i=0; i<L; i++){
		for (j=0; j<L; j++){
			for (k=0; k<L; k++){
				for (l=0; l<L; l++){
					for (mi=0; mi<4; mi++){
						if (gsl_rng_uniform(r)>0.5){
							*(p[i][j][k][l]+mi)=gsl_rng_uniform(r);
							*(p[i][j][k][l]+mi) /= 5;
						}
						else{
							*(p[i][j][k][l]+mi)=-gsl_rng_uniform(r);
							*(p[i][j][k][l]+mi) /= 5;
						}
					};			
				};
			};
		};
	};
	gauge(p);
	
	return 0;						
}


//double initializeb(){
		
