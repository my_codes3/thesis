#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <string.h>
#include <time.h>
#include <gsl/gsl_rng.h>
#include <omp.h>
//#include <gsl_rng.h>
// lf gsl_rng_uniform(r) to obtain a double. To obtain int: multiply by 2^42 and then take floor


typedef struct listg{
	int site[4];
	int var;
	struct listg* next;
	struct listg* prev;
} listg_t;

int L=8;
int N;
int V;
double beta=100;
double acc=0;
double makeint= pow(2.0, 42);
gsl_rng **r;
const gsl_rng_type *T;
int tot0=0;
int tot1=0;
int tot2=0;
int gaugetot=0;
int ptotgauge=0;
int chargetot=8;



double action(double *p[L][L][L][L]);
//double metropolis(double *p[L][L][L][L], double E, double beta);
int gauge(double *p[L][L][L][L]);
int initialize(double *p[L][L][L][L]);
int metropolis(double *p[L][L][L][L], int i, int j, int k, int l, int mi);
double multimeas(double *p[L][L][L][L], int thick, double charge, double _Complex *Wtilde[L/2][L][L][L]);
double multi(double *p[L][L][L][L], int thick);
double action_loc(double *p[L][L][L][L], int itmp, int jtmp, int ktmp, int ltmp);


int main(){
	printf("%lf \n", makeint);
	//srand(7);
	gsl_rng_env_setup();
	T= gsl_rng_ranlxd2;
	int timeslice;
	r = (gsl_rng **) malloc(L * sizeof(gsl_rng *));
	for (timeslice=0; timeslice<L; timeslice ++){	
		r[timeslice]=gsl_rng_alloc(T);
		gsl_rng_set(r[timeslice], 631209522 +timeslice*15182);
	}
	FILE *itp, *itpW1real, *itpacc, *itpdE, *itpE, *itpraw1;
	double M[L][L][L][L][4], *p[L][L][L][L], A, exp1, e[chargetot], Mtot[L][L][L][L][4], *ptot[L][L][L][L];
	clock_t start, t1, t2;
	double time_used;
	long double devE;
	int Nsweep=3000, Nsubsweep=100, m, index, Nthermalization=100000000, Nstored, ns, nj;  //provisional, just to recheck the code
	unsigned long long i, j, k, l, mi;
	double E[1000100], Etmp, W, varW1=0, thbis=0;    //for the actual code
	//double E[Nthermalization];		//to find tau_nl
	
	double _Complex avg=0, W1[Nsweep], Wtildevec[L/2][L][L][L], *Wtilde[L/2][L][L][L];
	//printf("ok\n");
	fflush(stdout);
	char title1[100]="wilson_L8_beta100_e", num[50], title2[100], title3[100]="raw_wilson_L8_beta100_e", title4[100], title5[100]="raw_energy_L8_beta100_e", title6[100]="lattice_L8_beta100_";
	N=pow(L,4);
	V=pow(L,3); 
	start=clock();
	for (i=0; i<chargetot; i++){
		e[i]=(4-0.0078)*i/7 +0.0078;
	}
	for (index=0; index<1; index++){
		strncpy(title2, title1, sizeof(title2));
		sprintf(num, "%f", e[index]);
		strcat(title2, num);
		strcat(title2, "_1.txt");
		strcat(title5, num);
		strcat(title5, "_1.txt");
		strncpy(title4, title3, sizeof(title4));
		strcat(title4, num);
		strcat(title4, "_bis.txt");
		strcat(title6, num);	
		strcat(title6, "_2.txt");
		for (i=0; i<L; i++){
			for (j=0; j<L; j++){
				for (k=0; k<L; k++){
					for (l=0; l<L; l++){
						p[i][j][k][l]=&M[i][j][k][l][0];
						//ptot[i][j][k][l]=&Mtot[i][j][k][l][0];
					};
				};
			};
		};
		for (i=0; i<L/2; i++){
			for (j=0; j<L; j++){
				for (k=0; k<L; k++){
					for (l=0; l<L; l++){
						Wtilde[i][j][k][l] = &Wtildevec[i][j][k][l];
					};
				};
			};
		};
		itp=fopen("./lattice_L8_beta100_11.txt", "r");
		for (i=0; i<L; i++){
			for (j=0; j<L; j++){
				for (k=0; k<L; k++){
					for (l=0; l<L; l++){
						for (mi=0; mi<4; mi++){
							fscanf(itp, "%lf", (p[i][j][k][l]+mi));
						}
					}
				}
			}
		}
		
		fclose(itp);
		
		//initialize(p);
		gauge(p);
		Etmp=action(p);		
		j=0;
		printf("here ok\n");
		
		/*for (i=0; i<Nthermalization; i++){
			if (i%100==0){
				Etmp=metropolis_nog(p,Etmp,beta);
				if (i%1000==0){
					gauge(p);
				}
				E[j]=Etmp;
				j++;
				t1=clock();
				time_used= ((double) (t1-start))/CLOCKS_PER_SEC;
				time_used /= 60;	//I get the minutes
				if (time_used > 1410){ 	//almost 24hours, suited for piz-daint normal computations
					break;
				}
			}
			else{
				Etmp=metropolis_nog(p,Etmp,beta);
				//printf("%lf\n", Etmp);
			}
		};
		
		Nstored=j;
		//printf("end therm\n");
		Etmp=action(p);
		
		
		if (index==0){
			itp=fopen("therm_b1_10.txt", "w");
			for (i=0; i<Nstored; i++){
				fprintf(itp, "%.12f\n", E[i]);
			}
			fclose(itp);
		}
		*/
		
		for(ns=0; ns<Nsweep; ns++){
			for (nj=0; nj<10; nj++){ //for now keep 1
				
				#pragma omp parallel shared(p) num_threads(4)
				{
					for (int mipar=0; mipar<4; mipar++){	
						#pragma omp for
						for (int ipar=0; ipar<L; ipar+=2){
							for (int jpar=0; jpar<L; jpar++){
								for (int kpar=0; kpar<L; kpar++){
									for (int lpar=0; lpar<L; lpar++){
										metropolis(p, ipar, jpar, kpar, lpar, mipar);
									}
								}
							}
						}
						
					
						#pragma omp barrier
						
						#pragma omp for
						for (int ipar=1; ipar<L; ipar+=2){
							for (int jpar=0; jpar<L; jpar++){
								for (int kpar=0; kpar<L; kpar++){
									for (int lpar=0; lpar<L; lpar++){
										metropolis(p, ipar, jpar, kpar, lpar, mipar);
									}
								}
							}
						}
						#pragma omp barrier
						if (omp_get_thread_num()==0){
						
						gauge(p);
						
						}
						#pragma omp barrier 
						
					
					}
				}
			}
			
			if (ns%100==0){	//I measure the wilson line and the field
				//#pragma omp parallel firstprivate(Mpar, Etmp) shared(E_par)
				
				for (i=0; i<L/2; i++){
					for (j=0; j<L; j++){
						for (k=0; k<L; k++){
							for (l=0; l<L; l++){
								*Wtilde[i][j][k][l] =0;
							}
						}
					}
				}
				
				//t1=clock();
				
				Etmp=multimeas(p, 2, e[index], Wtilde);
				avg=0;
				for (j=0; j<L; j++){
					for (k=0; k<L; k++){
						for (l=0; l<L; l++){
							avg += (*Wtilde[0][j][k][l])*(*Wtilde[1][j][k][l])*(*Wtilde[2][j][k][l])*(*Wtilde[3][j][k][l]);
						}
					}
				}
				avg /=V;
				itp=fopen(title2, "a");
				fprintf(itp, "%lf \t %lf\n", creal(avg), cimag(avg));
				fclose(itp);
				//if (index==0){
				Etmp=action(p);
				itp=fopen(title5, "a");
				fprintf(itp, "%lf\n", Etmp);
				fclose(itp);
				//}
				//t2=clock();
				//time_used= ((double) (t2 - t1))/CLOCKS_PER_SEC;
				//time_used /= 3600;
				//printf("time used in multimeas=%lf\n", time_used);
				/*
				for (i=0; i<L; i++){
					for (j=0; j<L; j++){
						for (k=0; k<L; k++){
							for (l=0; l<L; l++){
								*(ptot[i][j][k][l])  /= 1030;
								if (i%2 ==1){
									for (mi=1; mi<4; mi++){
										*(ptot[i][j][k][l]+mi)  /=220;
									}
								}
								else{
									for (mi=1; mi<4; mi++){
										*(ptot[i][j][k][l]+mi)  /=130;
									}
								}
							}
						}
					}
				}
				*/
				//gauge ptot?
				
				 
			}			
			else{
				
				//#pragma omp parallel firstprivate(Mpar, Etmp) shared(E_par) 
				//t1=clock();
				Etmp=multi(p, 2);
				//t2=clock();
				//time_used= ((double) (t2-t1))/CLOCKS_PER_SEC;
				//time_used /= 3600;
				//printf("time used in multi=%lf\n", time_used);
				
			}
			//printf("energy=%.12lf\n", Etmp);
			
			
							
		
		}
		printf("here ok\n");
		//printf("energy=%lf\n", Etmp);
		Etmp=action(p);
		//printf("energy=%lf\n", Etmp);
		/*
		itp=fopen("ptotal_2.txt", "w");
		for (i=0; i<L; i++){
			for (j=0; j<L; j++){
				for (k=0; k<L; k++){
					for (l=0; l<L; l++){
						for (mi=0; mi<4; mi++){
							fprintf(itp, "%.21f\n", *(ptot[i][j][k][l]+mi));
						}
					}
				}
			}
		}
		fclose(itp);
		*/
		
		//printf("tot0=%d, tot1=%d, tot2=%d, gaugetot=%d, ptotgauge=%d\n", tot0, tot1, tot2, gaugetot, ptotgauge);
		//Etmp = action(ptot);
		/*
		for (i=0; i<L; i++){
			for (j=0; j<L; j++){
				for (k=0; k<L; k++){
					for (l=0; l<L; l++){
						
						thbis += pow((*(ptot[i][j][k][l]) - *(ptot[(i+L-1)%L][j][k][l]) + *(ptot[i][j][k][l]+1) - *(ptot[i][(j+L-1)%L][k][l]+1) + *(ptot[i][j][k][l]+2) - *(ptot[i][j][(k+L-1)%L][l]+2) + *(ptot[i][j][k][l]+3) - *(ptot[i][j][k][(l+L-1)%L]+3)), 2);
					}
				}
			}
		}
		//th /= N;
		thbis/=N;
		printf("energy ptot= %lf, thbis = %lf\n", Etmp, thbis);
		*/
	}
	
	for (timeslice=0; timeslice<L; timeslice ++){	
		free(r[timeslice]);
	}
	itp=fopen(title6, "w");
	for (i=0; i<L; i++){
		for (j=0; j<L; j++){
			for (k=0; k<L; k++){
				for (l=0; l<L; l++){
					for (mi=0; mi<4; mi++){
						fprintf(itp, "%.21f\n", *(p[i][j][k][l]+mi));
					}
				}
			}
		}
	}
	fclose(itp);
	
	return 0;
}

double action_loc(double *p[L][L][L][L], int itmp, int jtmp, int ktmp, int ltmp){
	double S=0, f1, f2;
	int i, j, k, l, mi, ni, itmp1, jtmp1, ktmp1, ltmp1, itmp2, jtmp2, ktmp2, ltmp2, a, midual, nidual, itmp3, jtmp3, ktmp3, ltmp3, itmp4, jtmp4, ktmp4, ltmp4, c, iter;
	for (iter=0; iter <5; iter ++){
		switch (iter){
			case 0:
				i=itmp;
				j=jtmp;
				k=ktmp;
				l=ltmp;
			
			break;
			
			case 1:
				i=(itmp-1+L)%L;
				j=jtmp;
				k=ktmp;
				l=ltmp;
			
			
			break;
			
			case 2:
				i=itmp;
				j=(jtmp+L-1)%L;
				k=ktmp;
				l=ltmp;
			
			
			break;
			
			case 3:
				i=itmp;
				j=jtmp;
				k=(ktmp+L-1)%L;
				l=ltmp;
			break;
			
			case 4:
				i=itmp;
				j=jtmp;
				k=ktmp;
				l=(ltmp+L-1)%L;
			break;
		}
		f1=0;
		f2=0;
		for (mi=1; mi<4; mi++){
			if (mi==1){
				itmp1=i;
				jtmp1=(j+1)%L;
				ktmp1=k;
				ltmp1=l;
			};
			if (mi==2){
				itmp1=i;
				jtmp1=j;
				ktmp1=(k+1)%L;
				ltmp1=l;
			};
			if (mi==3){
				itmp1=i;
				jtmp1=j;
				ktmp1=k;
				ltmp1=(l+1)%L;
			};
			for (ni=0; ni<mi; ni++){
				if (ni==0){
					itmp2=(i+1)%L;
					jtmp2=j;
					ktmp2=k;
					ltmp2=l;
				};
				if (ni==1){
					itmp2=i;
					jtmp2=(j+1)%L;
					ktmp2=k;
					ltmp2=l;
				};
				if (ni==2){
					itmp2=i;
					jtmp2=j;
					ktmp2=(k+1)%L;
					ltmp2=l;
				};
				
				f1 += pow((*(p[itmp1][jtmp1][ktmp1][ltmp1]+ni) - *(p[i][j][k][l] + ni) + *(p[i][j][k][l] + mi) - *(p[itmp2][jtmp2][ktmp2][ltmp2]+mi)),2);
				//printf("f1=%lf \n", f1);
				
				//f1 = f1/4;

	// Here I am computing (F_mi_ni)^2=(A_ni(x+mi)-A_ni(x)+A_mi(x)-A_mi(x+ni))^2      where the "vector" tmp1=x+mi and tmp2=x+ni 
				
				midual=-1;
				for (a=0; a<4; a++){
					if (mi != a && ni != a){
						if (midual == -1){
							midual= a;
							if (midual==0){
								itmp3=(i+1)%L;
								jtmp3=j;
								ktmp3=k;
								ltmp3=l;
							};
							if (midual==1){
								itmp3=i;
								jtmp3=(j+1)%L;
								ktmp3=k;
								ltmp3=l;
							};
							if (midual==2){
								itmp3=i;
								jtmp3=j;
								ktmp3=(k+1)%L;
								ltmp3=l;
							};
							if (midual==3){
								itmp3=i;
								jtmp3=j;
								ktmp3=k;
								ltmp3=(l+1)%L;
							};
						}
						else{
							nidual= a;
							if (nidual==0){
								itmp4=(i+1)%L;
								jtmp4=j;
								ktmp4=k;
								ltmp4=l;
							};
							if (nidual==1){
								itmp4=i;
								jtmp4=(j+1)%L;
								ktmp4=k;
								ltmp4=l;
							};
							if (nidual==2){
								itmp4=i;
								jtmp4=j;
								ktmp4=(k+1)%L;
								ltmp4=l;
							};
							if (nidual==3){
								itmp4=i;
								jtmp4=j;
								ktmp4=k;
								ltmp4=(l+1)%L;
							};
						};
					};
				};
				c=-1;
				if (nidual==2 && midual==0){
					c=1;
				}
				if (nidual==3 && midual==1){
					c=1;
				}
				f2 +=  c*(*(p[itmp1][jtmp1][ktmp1][ltmp1]+ni) - *(p[i][j][k][l] + ni) + *(p[i][j][k][l] + mi) - *(p[itmp2][jtmp2][ktmp2][ltmp2]+mi))*(*(p[itmp3][jtmp3][ktmp3][ltmp3]+nidual) - *(p[i][j][k][l] + nidual) + *(p[i][j][k][l] + midual) - *(p[itmp4][jtmp4][ktmp4][ltmp4]+midual));
	//This is F_mi_ni* dual(F_mi_ni)
				//printf("f2=%lf\n", f2);
				
				
			};						
		};
		
		f2 = pow(f2, 2)/4;
		//f1 = 2*f1;	
		/*if(i==(L-2)){
			printf("f2=%lf \n", f2);
			printf("f1=%lf \n", f1);
		}*/
		S += (sqrt(1+ f1/beta + f2/(pow(beta, 2))) -1);
		
	}
	return beta*S;
}

int gauge(double *p[L][L][L][L]){
	int i, j ,k ,l, a, tot=0;
	double M[L][L][L][L][4], *ptmp[L][L][L][L];
	double th, thbis;
	double g[L][L][L][L], gvar;
	//time_t t1, time_used;
	th=0;
	thbis=0;
	for (i=0; i<L; i++){
		for (j=0; j<L; j++){
			for (k=0; k<L; k++){
				for (l=0; l<L; l++){
					g[i][j][k][l]=0;
				}
			}
		}
	}
	for (i=0; i<L; i++){
		for (j=0; j<L; j++){
			for (k=0; k<L; k++){
				for (l=0; l<L; l++){
					ptmp[i][j][k][l]=&M[i][j][k][l][0];
				};
			};
		};
	};
	for (i=0; i<L; i++){
		for (j=0; j<L; j++){
			for (k=0; k<L; k++){
				for (l=0; l<L; l++){
					*(ptmp[i][j][k][l]) = *(p[i][j][k][l]);
					*(ptmp[i][j][k][l]+1) = *(p[i][j][k][l]+1);
					*(ptmp[i][j][k][l]+2) = *(p[i][j][k][l]+2);
					*(ptmp[i][j][k][l]+3) = *(p[i][j][k][l]+3);
				};
			};
		};
	};
	for (i=0; i<L; i++){
		for (j=0; j<L; j++){
			for (k=0; k<L; k++){
				for (l=0; l<L; l++){
					thbis += pow((*(p[i][j][k][l]) - *(p[(i+L-1)%L][j][k][l]) + *(p[i][j][k][l]+1) - *(p[i][(j+L-1)%L][k][l]+1) + *(p[i][j][k][l]+2) - *(p[i][j][(k+L-1)%L][l]+2) + *(p[i][j][k][l]+3) - *(p[i][j][k][(l+L-1)%L]+3)), 2);
					//if (thbis>25000){
						
					//	printf("i=%d, j=%d, k=%d, l=%d\n", i, j, k, l);
					//	return 0;
					//}
				}
			}
		}
	}
	//th /= N;
	thbis/=N;
	//printf("thbis beg=%.12lf\n", thbis);
	while (thbis>0.01){
		/*t1=time(NULL);
		time_used= ((double) (t1-start))/60;
		if (time_used > 1260){ 	//almost 6 hours, suited for piz-daint normal computations
			printf("thbis=%lf\n", thbis);
			tot++;
			if (tot==2){
				break;
			}
		}
		*/
		for (i=0; i<L; i++){
			for (j=0; j<L; j++){
				for (k=0; k<L; k++){
					for (l=0; l<L; l++){
						gvar=0;
						gvar -= *(p[i][j][k][l]) - g[(i+1)%L][j][k][l] - *(p[(i+L-1)%L][j][k][l]) + g[(i+L-1)%L][j][k][l] ;
						gvar -= *(p[i][j][k][l]+1) - g[i][(j+1)%L][k][l] - *(p[i][(j+L-1)%L][k][l]+1) + g[i][(j+L-1)%L][k][l] ;
						gvar -= *(p[i][j][k][l]+2) - g[i][j][(k+1)%L][l] - *(p[i][j][(k+L-1)%L][l]+2) + g[i][j][(k+L-1)%L][l] ;
						gvar -= *(p[i][j][k][l]+3) - g[i][j][k][(l+1)%L] - *(p[i][j][k][(l+L-1)%L]+3) + g[i][j][k][(l+L-1)%L] ;
						g[i][j][k][l]=gvar/8;
						//printf("g=%lf + i%lf\n", creal(g[i][j][k][l]), cimag(g[i][j][k][l]));
					}
				}
			}
		}
		for (i=0; i<L; i++){
			for (j=0; j<L; j++){
				for (k=0; k<L; k++){
					for (l=0; l<L; l++){
						
						*(ptmp[i][j][k][l]) = *(p[i][j][k][l]) +g[i][j][k][l] - g[(i+1)%L][j][k][l];
						*(ptmp[i][j][k][l]+1) = *(p[i][j][k][l]+1) + g[i][j][k][l] - g[i][(j+1)%L][k][l];
						*(ptmp[i][j][k][l]+2) = *(p[i][j][k][l]+2) + g[i][j][k][l] - g[i][j][(k+1)%L][l];
						*(ptmp[i][j][k][l]+3) = *(p[i][j][k][l]+3) + g[i][j][k][l] - g[i][j][k][(l+1)%L];
						//printf("clog=%lf +i 
					}
				}
			}
		}
		
	
		//th=0;
		thbis=0;
		for (i=0; i<L; i++){
			for (j=0; j<L; j++){
				for (k=0; k<L; k++){
					for (l=0; l<L; l++){
						
						thbis += pow((*(ptmp[i][j][k][l]) - *(ptmp[(i+L-1)%L][j][k][l]) + *(ptmp[i][j][k][l]+1) - *(ptmp[i][(j+L-1)%L][k][l]+1) + *(ptmp[i][j][k][l]+2) - *(ptmp[i][j][(k+L-1)%L][l]+2) + *(ptmp[i][j][k][l]+3) - *(ptmp[i][j][k][(l+L-1)%L]+3)), 2);
						//printf("p=%lf\n", *p[i][j][k][l]);
					}
				}
			}
		}
		//th /= N;
		thbis /= N;
		//if(thbis>0.01){
			//printf("th=%.10Lf\n", th);
		//printf("thbis=%.10lf\n", thbis);
			//printf("Energy=%lf\n", action(ptmp));
			
		//}
	}
	//printf("thbis fin=%lf\n", thbis);		
	for (i=0; i<L; i++){
		for (j=0; j<L; j++){
			for (k=0; k<L; k++){
				for (l=0; l<L; l++){
					*(p[i][j][k][l]) = *(ptmp[i][j][k][l]);
					*(p[i][j][k][l]+1) = *(ptmp[i][j][k][l]+1);
					*(p[i][j][k][l]+2) = *(ptmp[i][j][k][l]+2);
					*(p[i][j][k][l]+3) = *(ptmp[i][j][k][l]+3);
				}
			}
		}
	}
			
	
	
	return 0;
		
	
}

double multimeas(double *p[L][L][L][L], int thick, double charge, double _Complex *Wtilde[L/2][L][L][L]){
	
	//double *ptotal[L][L][L][L], Mtotal[L][L][L][L][4]; like the sum of all the results?
	int itmp, jtmp, ktmp, ltmp, mimp, iterout;
	//double Mpar[L][L][L][L][4], Etmp;
	double Etmp;
	/*
	for (itmp=0; itmp<L; itmp++){
		for (jtmp=0; jtmp<L; jtmp++){
			for (ktmp=0; ktmp<L; ktmp++){
				for (ltmp=0; ltmp<L; ltmp++){
					for (mimp=0; mimp<4; mimp++){
						Mpar[itmp][jtmp][ktmp][ltmp][mimp]=*(p[itmp][jtmp][ktmp][ltmp]+mimp);
						
					}
				}
			}
		}
	}
	*/
	if (thick==1){
		int iter2;
		double _Complex Utot[L][L][L][L];
		//double A0eff[L][L][L][L];
		for (itmp=0; itmp<L; itmp ++){
			for (jtmp=0; jtmp<L; jtmp ++){
				for (ktmp=0; ktmp<L; ktmp ++){
					for (ltmp=0; ltmp<L; ltmp++){
						Utot[itmp][jtmp][ktmp][ltmp]=0;
					}
				}
			}
		}
		for (iter2=0; iter2<10; iter2++){
			//#pragma omp parallel firstprivate(Mpar) shared(p, ptotal) num_threads(8)
			#pragma omp parallel shared(p, charge) num_threads(8)
			{
				//double *ppar[L][L][L][L];
				int iter, ib, j, k, l, mi;
				double _Complex avg1;
				double A0V;
				/*
				for (ib=0; ib<L; ib++){
					for (j=0; j<L; j++){
						for (k=0; k<L; k++){
							for (l=0; l<L; l++){
								ppar[ib][j][k][l]=&Mpar[ib][j][k][l][0];
								for (mi=0; mi<4; mi++){
									*(ppar[ib][j][k][l]+mi) = *(p[ib][j][k][l]+mi);
								}
							};
						};
					};
				};
				*/
				
				
				
				#pragma omp for
			
				for (int i=0; i<L; i++){
					/*
					if (i==0){
						ptotgauge +=1;
					}
					
					for (j=0; j<L; j++){
						for (k=0; k<L; k++){
							for (l=0; l<L; l++){
								for (mi=1; mi<4; mi++){
									*(ptotal[i][j][k][l]+mi) += *(p[i][j][k][l]+mi);
									if (i==1 && j==0 && k==0 && l==0 && mi ==1){
								
										tot1 +=1;	
									}
									if (i==0 && j==0 && k==0 && l==0 && mi==1){
										tot2 +=1;
									}
								}
							}
						}
					}
					*/				
					for (j=0; j<L; j++){
						
						for (k=0; k<L; k++){
							for (l=0; l<L; l++){
								//avg1 = 0;
								A0V=0;
						
								for (int jav=0; jav<L; jav++){
									for (int kav=0; kav<L; kav++){
										for (int lav=0; lav<L; lav++){
										
											A0V -= charge*(*(p[i][jav][kav][lav]));
										}
									}
								}
								A0V += charge*(*(p[i][j][k][l]));			
								A0V /= V;
								for (iter=0; iter<10; iter ++){
									metropolis(p, i, j, k, l, 0);
									Utot[i][j][k][l] += cexp(I*(charge*(*(p[i][j][k][l])) + A0V));
									//avg1 += cexp(I*(*(p[i][j][k][l])));
									/*(ptotal[i][j][k][l]) += *(p[i][j][k][l]);
									
									if (i==0 && j==0 && k==0 && l==0){
										//printf("i=%d, j=%d, k=%d, l=%d, iter= %d, iter2=%d, thread=%d \n", i, j, k, l, iter, iter2, omp_get_thread_num());
									
										tot0 +=1;	
									}
									*/
									
								}
								
								//*(ptotal[i][j][k][l]) += avg1/10;
								
							}
						}
					}
					/*
					for (j=0; j<L; j++){
						for (k=0; k<L; k++){
							for (l=0; l<L; l++){
								*(p[i][j][k][l])=*(ppar[i][j][k][l]);
							}
						}
					}
					*/
				}
				#pragma omp barrier
				if (omp_get_thread_num()==0){
					/*
					double thbis=0;
					for (int it=0; it<L; it++){
						for (int jt=0; jt<L; jt++){
							for (int kt=0; kt<L; kt++){
								for (int lt=0; lt<L; lt++){
									
									thbis += pow((*(p[it][jt][kt][lt]) - *(p[(it+L-1)%L][jt][kt][lt]) + *(p[it][jt][kt][lt]+1) - *(p[it][(jt+L-1)%L][kt][lt]+1) + *(p[it][jt][kt][lt]+2) - *(p[it][jt][(kt+L-1)%L][lt]+2) + *(p[it][jt][kt][lt]+3) - *(p[it][jt][kt][(lt+L-1)%L]+3)), 2);
								}
							}
						}
					}
					//th /= N;
					thbis/=N;
					printf("thbis 1=%lf\n", thbis);
					*/
					gauge(p);
					gaugetot +=1;
					// put something in ptotal? no
				}
				#pragma omp barrier 
			}
				
				/*
				for (int ic=0; ic<L; ic++){
					for (j=0; j<L; j++){
						for (k=0; k<L; k++){
							for (l=0; l<L; l++){
								for (mi=0; mi<4; mi++){
									*(ppar[ic][j][k][l]+mi)=*(p[ic][j][k][l]+mi);
								}
							}
						}
					}
				}
				#pragma omp barrier
				*/
					
			
				

				
			
								
			
			/*
			#pragma omp barrier
			if (omp_get_thread_num()==0){
				gauge(p);
				// put something in ptotal?
			}
			#pragma omp barrier
			*/
			
		
		}
		// compute action
		//Etmp=action(p);
		//gauge(p);
		for (itmp=0; itmp<L/2; itmp ++){
			for (jtmp=0; jtmp<L; jtmp ++){
				for (ktmp=0; ktmp<L; ktmp ++){
					for (ltmp=0; ltmp<L; ltmp++){
						*(Wtilde[itmp][jtmp][ktmp][ltmp]) += ((Utot[2*itmp][jtmp][ktmp][ltmp]/100)*(Utot[2*itmp +1][jtmp][ktmp][ltmp]/100))/10;
					}
				}
			}
		}
		return 0;
	
	}
	
	else{
		for (iterout=0; iterout<10; iterout ++){
			multimeas(p, 1, charge, Wtilde);
			//gauge(p);
			//gaugetot += 1;
			#pragma omp parallel shared(p, Wtilde) num_threads(4)
			{
				//double *ppar[L][L][L][L];
				int iter, ib, j, k, l, mi, mibis;
				/*
				for (ib=0; ib<L; ib++){
					for (j=0; j<L; j++){
						for (k=0; k<L; k++){
							for (l=0; l<L; l++){
								ppar[ib][j][k][l]=&Mpar[ib][j][k][l][0];
							};
						};
					};
				};
				*/
				for (mi=1; mi<4; mi++){
					
					#pragma omp for 					
					for (int i=0; i<(L/2); i++){
						/*
						if (i==0){
							ptotgauge +=1;
						}
						
						for (j=0; j<L; j++){
							for (k=0; k<L; k++){
								for (l=0; l<L; l++){	
									for (mibis=0; mibis <4; mibis++){
										if (mibis==mi){
											*(ptotal[2*i][j][k][l]+mibis) += *(p[2*i][j][k][l]+mibis);
											if (i==0 && j==0 && k==0 && l==0 && mibis==1){
												tot2 +=1;
											}
										}
										else{
											*(ptotal[2*i][j][k][l]+mibis) += *(p[2*i][j][k][l]+mibis);
											*(ptotal[2*i +1][j][k][l]+mibis) += *(p[2*i +1][j][k][l]+mibis);
											if (i==0 && j==0 && k==0 && l==0 && mibis==0){
												tot0 +=1;
											}
											if (i==1 && j==0 && k==0 && l==0 && mibis ==1){
										
												tot1 +=1;	
											}
											if (i==0 && j==0 && k==0 && l==0 && mibis==1){
												tot2 +=1;
											}
										}
										
									}
								}
							}
						}
						*/
						for (j=0; j<L; j++){
							for (k=0; k<L; k++){
								for (l=0; l<L; l++){	
									for (iter=0; iter<10; iter++){
										metropolis(p, 2*i +1, j, k, l, mi);
										/*
										*(ptotal[2*i +1][j][k][l]+mi) += *(p[2*i +1][j][k][l]+mi);
										if (i==0 && j==0 && k==0 && l==0 && mi ==1){
										
											tot1 +=1;	
										}
										*/
										
																				
									}
								}
								
							}
							
						}
						/*
						for (j=0; j<L; j++){
							for (k=0; k<L; k++){
								for (l=0; l<L; l++){
									*(p[2*i+1][j][k][l]+mi)=*(ppar[2*i+1][j][k][l]+mi);
								}
							}
						}
						*/
					}
					#pragma omp barrier
					if (omp_get_thread_num()==0){
						/*
						double thbis=0;
						for (int it=0; it<L; it++){
							for (int jt=0; jt<L; jt++){
								for (int kt=0; kt<L; kt++){
									for (int lt=0; lt<L; lt++){
										
										thbis += pow((*(p[it][jt][kt][lt]) - *(p[(it+L-1)%L][jt][kt][lt]) + *(p[it][jt][kt][lt]+1) - *(p[it][(jt+L-1)%L][kt][lt]+1) + *(p[it][jt][kt][lt]+2) - *(p[it][jt][(kt+L-1)%L][lt]+2) + *(p[it][jt][kt][lt]+3) - *(p[it][jt][kt][(lt+L-1)%L]+3)), 2);
									}
								}
							}
						}
						//th /= N;
						thbis/=N;
						printf("thbis 2=%lf\n", thbis);					
						*/
						gauge(p);
						//gaugetot +=1;
						// put something in ptotal?
					}
					#pragma omp barrier 
					/*
					for (int ic=0; ic<L; ic++){ 
						for (j=0; j<L; j++){
							for (k=0; k<L; k++){
								for (l=0; l<L; l++){
									for (mibis=0; mibis<4; mibis++){
										*(ppar[ic][j][k][l]+mibis)=*(p[ic][j][k][l]+mibis);
									}
								}
							}
						}
					}
					*/
					//#pragma omp barrier
					
				}
					/*
					for (mi=1; mi<4; mi++){
						for (j=0; j<L; j++){
							for (k=0; k<L; k++){
								for (l=0; l<L; l++){
									*(p[2*i +1][j][k][l]+mi)=*(ppar[2*i +1][j][k][l]+mi);
								}
							}
						}
					}
					*/
				
			}
				
				
			
			//Etmp=action(p);
			//compute action
			//gauge(p);
			//gaugetot +=1;
		}
		Etmp=action(p);
		//divide by 10*10 the time link, by 10 the ones in the spacial directions I update
		return Etmp;
	}
			

}

double multi(double *p[L][L][L][L], int thick){
	
	//double *ptotal[L][L][L][L], Mtotal[L][L][L][L][4]; like the sum of all the results?
	int itmp, jtmp, ktmp, ltmp, mimp, iterout;
	double Etmp;
	/*double Mpar[L][L][L][L][4], Etmp;
	for (itmp=0; itmp<L; itmp++){
		for (jtmp=0; jtmp<L; jtmp++){
			for (ktmp=0; ktmp<L; ktmp++){
				for (ltmp=0; ltmp<L; ltmp++){
					for (mimp=0; mimp<4; mimp++){
						Mpar[itmp][jtmp][ktmp][ltmp][mimp]=*(p[itmp][jtmp][ktmp][ltmp]+mimp);
						
					}
				}
			}
		}
	}
	*/
	if (thick==1){
		int iter2;
		for (iter2=0; iter2<10; iter2++){
			#pragma omp parallel shared(p) num_threads(8)
			{
				//double *ppar[L][L][L][L];
				int iter, ib, j, k, l, mi;
				/*
				for (ib=0; ib<L; ib++){
					for (j=0; j<L; j++){
						for (k=0; k<L; k++){
							for (l=0; l<L; l++){
								ppar[ib][j][k][l]=&Mpar[ib][j][k][l][0];
								for (mi=0; mi<4; mi++){
									*(ppar[ib][j][k][l]+mi) = *(p[ib][j][k][l]+mi);
								}
							};
						};
					};
				};
				*/
			
				#pragma omp for
			
				for (int i=0; i<L; i++){
					for (j=0; j<L; j++){
						
						for (k=0; k<L; k++){
							for (l=0; l<L; l++){
								for (iter=0; iter<10; iter ++){
									metropolis(p, i, j, k, l, 0);
									// *(ptotal[i][j][k][l]) += *(ppar[i][j][k][l]);
									
									/*if (i==0 && j==0 && k==0 && l==0){
										//printf("i=%d, j=%d, k=%d, l=%d, iter= %d, iter2=%d, thread=%d \n", i, j, k, l, iter, iter2, omp_get_thread_num());
									
										tot0 +=1;	
									}
									*/
									
								}
							}
						}
					}
				
					/*
					for (j=0; j<L; j++){
						for (k=0; k<L; k++){
							for (l=0; l<L; l++){
								*(p[i][j][k][l])=*(ppar[i][j][k][l]);
							}
						}
					}
					*/
				}
				#pragma omp barrier
				if (omp_get_thread_num()==0){
					gauge(p);
					// put something in ptotal? no
				}
				#pragma omp barrier 
			}
				
				/*
				for (int ic=0; ic<L; ic++){
					for (j=0; j<L; j++){
						for (k=0; k<L; k++){
							for (l=0; l<L; l++){
								for (mi=0; mi<4; mi++){
									*(ppar[ic][j][k][l]+mi)=*(p[ic][j][k][l]+mi);
								}
							}
						}
					}
				}
				#pragma omp barrier
				*/
					
			
				

				
			
								
			
			/*
			#pragma omp barrier
			if (omp_get_thread_num()==0){
				gauge(p);
				// put something in ptotal?
			}
			#pragma omp barrier
			*/
			
		
		}
		// compute action
		//Etmp=action(p);
		//gauge(p);
		return 0;
	
	}
	
	else{
		for (iterout=0; iterout<10; iterout ++){
			multi(p, 1);
			gauge(p);
			
			#pragma omp parallel shared(p) num_threads(4)
			{
				//double *ppar[L][L][L][L];
				int iter, ib, j, k, l, mi, mibis;
				/*
				for (ib=0; ib<L; ib++){
					for (j=0; j<L; j++){
						for (k=0; k<L; k++){
							for (l=0; l<L; l++){
								ppar[ib][j][k][l]=&Mpar[ib][j][k][l][0];
							};
						};
					};
				};
				*/
				for (mi=1; mi<4; mi++){
					#pragma omp for 					
					for (int i=0; i<(L/2); i++){
						for (j=0; j<L; j++){
							for (k=0; k<L; k++){
								for (l=0; l<L; l++){	
									for (iter=0; iter<10; iter++){
										metropolis(p, 2*i +1, j, k, l, mi);
										// *(ptotal[2*i +1][j][k][l]+mi) += *(ppar[2*i +1][j][k][l]+mi);
										/*
										if (i==0 && j==0 && k==0 && l==0 && mi ==1){
										
											tot1 +=1;	
										}
										*/
										
																				
									}
								}
								
							}
							
						}
						/*
						for (j=0; j<L; j++){
							for (k=0; k<L; k++){
								for (l=0; l<L; l++){
									*(p[2*i+1][j][k][l]+mi)=*(ppar[2*i+1][j][k][l]+mi);
								}
							}
						}
						*/
					}
					#pragma omp barrier
					if (omp_get_thread_num()==0){
						gauge(p);
						// put something in ptotal?
					}
					#pragma omp barrier 
					/*
					for (int ic=0; ic<L; ic++){ 
						for (j=0; j<L; j++){
							for (k=0; k<L; k++){
								for (l=0; l<L; l++){
									for (mibis=0; mibis<4; mibis++){
										*(ppar[ic][j][k][l]+mibis)=*(p[ic][j][k][l]+mibis);
									}
								}
							}
						}
					}
					#pragma omp barrier
					*/
					
				}
					/*
					for (mi=1; mi<4; mi++){
						for (j=0; j<L; j++){
							for (k=0; k<L; k++){
								for (l=0; l<L; l++){
									*(p[2*i +1][j][k][l]+mi)=*(ppar[2*i +1][j][k][l]+mi);
								}
							}
						}
					}
					*/
				
			}
				
				
			
			//Etmp=action(p);
			//compute action
			gauge(p);
		}
		Etmp=action(p);
		//divide by 10*10 the time link, by 10 the ones in the spacial directions I update
		return Etmp;
	}
			

}




int metropolis(double *p[L][L][L][L], int i, int j, int k, int l, int mi){
	double E_loc_new, E_loc_old, M[L][L][L][L][4], *ptmp[L][L][L][L], dA, q, expon;
	int itmp, jtmp, ltmp, ktmp, mitmp;
	for (itmp=0; itmp<L; itmp++){
		for (jtmp=0; jtmp<L; jtmp++){
			for (ktmp=0; ktmp<L; ktmp++){
				for (ltmp=0; ltmp<L; ltmp++){
					ptmp[itmp][jtmp][ktmp][ltmp]=&M[itmp][jtmp][ktmp][ltmp][0];
					for (mitmp=0; mitmp<4; mitmp++){
						*(ptmp[itmp][jtmp][ktmp][ltmp]+mitmp) = *(p[itmp][jtmp][ktmp][ltmp]+mitmp);
					};
				};
			};
		};
	};
	E_loc_old=action_loc(p, i, j, k, l);
	if (gsl_rng_uniform(r[i])>0.5){
		dA=gsl_rng_uniform(r[i]);		
	}
	else{
		dA=-gsl_rng_uniform(r[i]);
	}
	dA /= 50;
	*(ptmp[i][j][k][l]+mi) += dA;
	E_loc_new=action_loc(ptmp, i, j, k, l);
	if (E_loc_new<=E_loc_old){

		*(p[i][j][k][l]+mi)=*(ptmp[i][j][k][l]+mi);
		return 0;
	}
	
	else{
		q=gsl_rng_uniform(r[i]);
		if (q<exp(E_loc_old-E_loc_new)){
			*(p[i][j][k][l]+mi)=*(ptmp[i][j][k][l]+mi);
		
			return 0;
		};
		return 0;
	};
	return 0;
}







double action(double *p[L][L][L][L]){
	double S=0, f1, f2;
	int i, j, k, l, mi, ni, itmp1, jtmp1, ktmp1, ltmp1, itmp2, jtmp2, ktmp2, ltmp2, a, midual, nidual, itmp3, jtmp3, ktmp3, ltmp3, itmp4, jtmp4, ktmp4, ltmp4, c;
	for (i=0; i<L; i++){
		for (j=0; j<L; j++){ 
			for (k=0; k<L; k++){
				for (l=0; l<L; l++){
					f1=0;
					f2=0;
					for (mi=1; mi<4; mi++){
						if (mi==1){
							itmp1=i;
							jtmp1=(j+1)%L;
							ktmp1=k;
							ltmp1=l;
						};
						if (mi==2){
							itmp1=i;
							jtmp1=j;
							ktmp1=(k+1)%L;
							ltmp1=l;
						};
						if (mi==3){
							itmp1=i;
							jtmp1=j;
							ktmp1=k;
							ltmp1=(l+1)%L;
						};
						for (ni=0; ni<mi; ni++){
							if (ni==0){
								itmp2=(i+1)%L;
								jtmp2=j;
								ktmp2=k;
								ltmp2=l;
							};
							if (ni==1){
								itmp2=i;
								jtmp2=(j+1)%L;
								ktmp2=k;
								ltmp2=l;
							};
							if (ni==2){
								itmp2=i;
								jtmp2=j;
								ktmp2=(k+1)%L;
								ltmp2=l;
							};
							
							f1 += pow((*(p[itmp1][jtmp1][ktmp1][ltmp1]+ni) - *(p[i][j][k][l] + ni) + *(p[i][j][k][l] + mi) - *(p[itmp2][jtmp2][ktmp2][ltmp2]+mi)),2);
							//printf("f1=%lf \n", f1);
							
							//f1 = f1/4;

// Here I am computing (F_mi_ni)^2=(A_ni(x+mi)-A_ni(x)+A_mi(x)-A_mi(x+ni))^2      where the "vector" tmp1=x+mi and tmp2=x+ni 
							
							midual=-1;
							for (a=0; a<4; a++){
								if (mi != a && ni != a){
									if (midual == -1){
										midual= a;
										if (midual==0){
											itmp3=(i+1)%L;
											jtmp3=j;
											ktmp3=k;
											ltmp3=l;
										};
										if (midual==1){
											itmp3=i;
											jtmp3=(j+1)%L;
											ktmp3=k;
											ltmp3=l;
										};
										if (midual==2){
											itmp3=i;
											jtmp3=j;
											ktmp3=(k+1)%L;
											ltmp3=l;
										};
										if (midual==3){
											itmp3=i;
											jtmp3=j;
											ktmp3=k;
											ltmp3=(l+1)%L;
										};
									}
									else{
										nidual= a;
										if (nidual==0){
											itmp4=(i+1)%L;
											jtmp4=j;
											ktmp4=k;
											ltmp4=l;
										};
										if (nidual==1){
											itmp4=i;
											jtmp4=(j+1)%L;
											ktmp4=k;
											ltmp4=l;
										};
										if (nidual==2){
											itmp4=i;
											jtmp4=j;
											ktmp4=(k+1)%L;
											ltmp4=l;
										};
										if (nidual==3){
											itmp4=i;
											jtmp4=j;
											ktmp4=k;
											ltmp4=(l+1)%L;
										};
									};
								};
							};
							c=-1;
							if (nidual==2 && midual==0){
								c=1;
							}
							if (nidual==3 && midual==1){
								c=1;
							}
							f2 +=  c*(*(p[itmp1][jtmp1][ktmp1][ltmp1]+ni) - *(p[i][j][k][l] + ni) + *(p[i][j][k][l] + mi) - *(p[itmp2][jtmp2][ktmp2][ltmp2]+mi))*(*(p[itmp3][jtmp3][ktmp3][ltmp3]+nidual) - *(p[i][j][k][l] + nidual) + *(p[i][j][k][l] + midual) - *(p[itmp4][jtmp4][ktmp4][ltmp4]+midual));
			//This is F_mi_ni* dual(F_mi_ni)
							//printf("f2=%lf\n", f2);
							
							
						};						
					};
					
					f2 = pow(f2, 2)/4;
					//f1 = 2*f1;	
					/*if(i==(L-2)){
						printf("f2=%lf \n", f2);
						printf("f1=%lf \n", f1);
					}*/
					S += (sqrt(1+ f1/beta + f2/(pow(beta, 2))) -1);
					//printf("S=%lf\n", S);
					//S += (sqrt(1+f1/beta) -1);
					
					/*if(i==(L-2) && j==4 && k==2 && l==(L-1)){
						printf("S partial before= %lf \n", S);
					}*/
					/*if(i==(L-2)){
						printf("S partial after= %lf \n", S);
					}*/
				};
			};
		};
	};
	//printf("S*beta= %lf\n", beta*S);
	return beta*S;
}

int initialize(double *p[L][L][L][L]){
	/*  this function initializes a lattice of size L 
		with Landau gauge choice, sum(A_j(x+j))-A_j)=0*/
	int i,j,mi,k,l;
	//double gaugev;
	for (i=0; i<L; i++){
		for (j=0; j<L; j++){
			for (k=0; k<L; k++){
				for (l=0; l<L; l++){
					for (mi=0; mi<4; mi++){
						if (gsl_rng_uniform(r[i])>0.5){
							*(p[i][j][k][l]+mi)=gsl_rng_uniform(r[i]);
							*(p[i][j][k][l]+mi) /= 5;
						}
						else{
							*(p[i][j][k][l]+mi)=-gsl_rng_uniform(r[i]);
							*(p[i][j][k][l]+mi) /= 5;
						}
					};			
				};
			};
		};
	};
	gauge(p);
	
	return 0;						
}


//double initializeb(){
		
