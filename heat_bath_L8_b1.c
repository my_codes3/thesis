
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <string.h>
#include <time.h>
#include <gsl/gsl_rng.h>
//#include <gsl_rng.h>
// lf gsl_rng_uniform(r) to obtain a double. To obtain int: multiply by 2^42 and then take floor

#define pi 3.1415927 

typedef struct listg{
	int site[4];
	int var;
	struct listg* next;
	struct listg* prev;
} listg_t;

int L=8;
int N;
int V;
double beta=1;
double acc=0;
double makeint= pow(2.0, 42);
gsl_rng *r;
const gsl_rng_type *T;

double DE_max=-1000;
double DE_min=1000;

double action(double *p[L][L][L][L]);
double heat(double *p[L][L][L][L], double E, double beta);
int gauge(double *p[L][L][L][L]);


double multconst=pow(2.9, 8);

int main(){
	//srand(7);
	gsl_rng_env_setup();
	T= gsl_rng_ranlxd2;
	r=gsl_rng_alloc(T);
	gsl_rng_set(r, 189413038);
	FILE *itp, *itpW1real, *itpacc, *itpdE, *itpE, *itpraw1;
	double M[L][L][L][L][4], *p[L][L][L][L], A, exp1, e[8];
	
	long double devE;
	int Nsweep=0, Nsubsweep=100, index, m, Nthermalization=1000000;  //provisional, just to recheck the code
	unsigned long long i, j, k, l, mi;
	double E[Nsweep], Etmp, W, varW1=0;    //for the actual code
	//double E[Nthermalization];		//to find tau_nl
	clock_t start, t1;
	double time_used;
	double _Complex avgW1=0, W1[Nsweep];
	printf("ok\n");
	fflush(stdout);
	char title1[100]="wilson1_L8_beta1_e", num[50], title2[100], title3[100]="raw_wilson1_L8_beta1_e", title4[100], title5[100]="raw_energy_L8_beta1.txt", title8[100]="lattice_L8_beta1_heat2.txt", title6[100]="proposed_DS_b1.txt";
	N=pow(L,4);
	V=pow(L,3); 
	start=clock();
	for (i=0; i<8; i++){
		e[i]=(4-0.0078)*i/7 +0.0078;
	}
	for (index=0; index<1; index++){
		strncpy(title2, title1, sizeof(title2));
		sprintf(num, "%f", e[index]);
		strcat(title2, num);
		strcat(title2, ".txt");
		strncpy(title4, title3, sizeof(title4));
		strcat(title4, num);
		strcat(title4, ".txt");
		printf("ok1\n");	
		for (i=0; i<L; i++){
			for (j=0; j<L; j++){
				for (k=0; k<L; k++){
					for (l=0; l<L; l++){
						p[i][j][k][l]=&M[i][j][k][l][0];
					};
				};
			};
		};

		itp=fopen("./lattice_L8_beta1.txt", "r");
		for (i=0; i<L; i++){
			for (j=0; j<L; j++){
				for (k=0; k<L; k++){
					for (l=0; l<L; l++){
						for (mi=0; mi<4; mi++){
							fscanf(itp, "%lf", (p[i][j][k][l]+mi));
						}
					}
				}
			}
		}
		printf("ok2\n");
		fclose(itp);
		Etmp=action(p);		
		itp=fopen(title5, "w");
		fprintf(itp, "\n");
		for (i=0; i<Nthermalization; i++){
			Etmp=heat(p,Etmp,beta);
			//printf("%lf\n", Etmp);
			fprintf(itp, "%.12f\n", Etmp);
			t1=clock();
			time_used= ((double) (t1-start))/CLOCKS_PER_SEC;
			time_used /= 60;	//I get the minutes
			if (time_used > 300){ 	//almost 24hours, suited for piz-daint normal computations
				break;
			}
		};
		Etmp=action(p);
		fclose(itp);
		itp=fopen(title8, "w");
		for (i=0; i<L; i++){
			for (j=0; j<L; j++){
				for (k=0; k<L; k++){
					for (l=0; l<L; l++){
						for (mi=0; mi<4; mi++){
							fprintf(itp, "%.21f\n", *(p[i][j][k][l]+mi));
						}
					}
				}
			}
		}
		fclose(itp);
		itp=fopen(title6,"w");
		fprintf(itp, "DE_max = %.21f\n", DE_max);
		fprintf(itp, "DE_min = %.21f\n", DE_min);
		fclose(itp);
		//printf("ok3\n");
		/*
		E[0]=Etmp;
		avgW1=0;
		W1[0]=0;
		A=0;
		for (m=0; m<L; m++){
			for (j=0; j<L; j++){	
				for (k=0; k<L; k++){
					for (l=0; l<L; l++){
						A += *(p[m][j][k][l]+3);
					}
				}
			}
		}
		//printf("ok4\n");
		for (m=0; m<L; m++){
			for (j=0; j<L; j++){	
				for (k=0; k<L; k++){
					//W1[i][m][j][k]=0;
					//W2[i][m][j][k]=0;
					exp1 = -A/V;
					for (l=0; l<L; l++){
						exp1 += *(p[m][j][k][l]+3);
					}
					//printf("exp1=%lf\n", exp1);
					//W1[i][m][j][k]=exp(e[index]*I*exp1);
					//W2[i][m][j][k] = exp(e[index]*I*exp1);
					//avgW1 += cexp(e[index]*I*exp1);
					W1[0] += cexp(e[index]*I*exp1);
				}
			}
		}
		//printf("ok5\n");
		W1[0] /= V;
		//avgW2=0;
		//E[499009]=Etmp;
		for (i=0; i<Nsweep; i++){
			//printf("%d 1\n", i);
			W1[i]=0;
			//printf("i=%d \n", i);
			Etmp=E[i-1];
			//printf("ok5bis\n");
			for (j=0; j<Nsubsweep; j++){
				Etmp=heat(p,Etmp,beta);
			}
			//printf("ok6\n");
			//printf("%d 2\n", i);
			E[i]=Etmp;
			A=0;
			for (m=0; m<L; m++){
				for (j=0; j<L; j++){	
					for (k=0; k<L; k++){
						for (l=0; l<L; l++){
							A += *(p[m][j][k][l]+3);
						}
					}
				}
			}
			//printf("ok7\n");
			//printf("%d 3\n", i);
			//printf("A=%lf\n", A);
			for (m=0; m<L; m++){
				for (j=0; j<L; j++){	
					for (k=0; k<L; k++){
						//W1[i][m][j][k]=0;
						//W2[i][m][j][k]=0;
						exp1 = -A/V;
						for (l=0; l<L; l++){
							exp1 += *(p[m][j][k][l]+3);
						}
						//printf("exp1=%lf\n", exp1);
						//W1[i][m][j][k]=exp(e[index]*I*exp1);
						//W2[i][m][j][k] = exp(e[index]*I*exp1);
						//avgW1 += cexp(e[index]*I*exp1);
						if (isnan(creal(avgW1))!=0){
							//printf("m= %d, j=%d, k=%d \n", m, j, k);
						}
						W1[i] += cexp(e[index]*I*exp1);
						//avgW2 += exp(e[index]*I*exp1);
					}
				}
			}
			
			//printf("%d 4\n", i);
			W1[i] /= V;
			//printf("ok8\n");
			 
			//printf("E[%d]=%lf \n", i, E[i]);
			
		};
		printf("okfuori\n");
		//avgW1 /= (V*Nsweep);
		//avgW2 /= V;
		for (i=0; i<Nsweep; i++){
			avgW1 += W1[i];
		}
		avgW1 /= Nsweep;
		
		for (i=0; i<Nsweep; i++){
			varW1 += pow(creal(W1[i]-avgW1),2) + pow(cimag(W1[i]-avgW1),2);
		}
		
		//varW1 /= (Nsweep-1)*Nsweep;
		//varW1 = sqrt(varW1);
		if (index==0){
			itp=fopen("Energy_L8_beta1.txt", "w");
			A=0;
			for (i=0; i<Nsweep; i++){
				A += E[i];				
			}
			A /= Nsweep;
			for (i=0; i<Nsweep; i++){
				devE += pow((A - E[i]), 2);
			}
			
			//devE /= (Nsweep*(Nsweep-1));
			if (devE<0){
				printf("the variance is negative!!\n");
			}
			//devE = sqrt(devE); 
			A /= N;
			devE /= N;
			devE /=N;
			fprintf(itp, "%f\n", A);
			fprintf(itp, "%.12Lf\n", devE);
		}
		itpW1real=fopen(title2, "w");
		W=creal(avgW1);
		fprintf(itpW1real, "%f\n", W);
		W=sqrt(pow(creal(avgW1),2)+pow(cimag(avgW1),2));
		fprintf(itpW1real, "%f\n", W);
		fprintf(itpW1real, "%.12f\n", varW1);
		fclose(itp);
		itp=fopen(title4, "w");
		for (i=0; i<(Nsweep-1); i++){
			fprintf(itp, "%.12f\t%.12f\n", creal(W1[i]), cimag(W1[i]));
		}
		fclose(itp);
		
		if (index==0){
			itp=fopen(title5, "w");
			for (i=0; i<(Nsweep-1); i++){
				fprintf(itp, "%.12f\n", E[i]);
			}
			fclose(itp);
		}
		
	*/	
	}
	
	return 0;
}
int gauge(double *p[L][L][L][L]){
	int ptmp[L][L][L][L], i, j ,k ,l, a;
	listg_t *head, *tmp, *tmp1;
	for (i=0; i<L; i++){
		for (j=0; j<L; j++){
			for (k=0; k<L; k++){
				for (l=0; l<L; l++){
					ptmp[i][j][k][l]=0;
				};
			};
		};
	};
	ptmp[0][0][0][0]=1;
	head=malloc(sizeof(listg_t));
	tmp=head;
	tmp1=head;
	tmp->site[0]=0;
	tmp->site[1]=0;
	tmp->site[2]=0;
	tmp->site[3]=0;
	tmp->var=0;
	tmp->prev=NULL;
	for (a=1; a<N; a++){
		tmp->next=malloc(sizeof(listg_t));
		i=tmp->site[0];
		j=tmp->site[1];
		k=tmp->site[2];
		l=tmp->site[3];
		tmp=tmp->next;
		tmp->next=NULL;
		tmp->site[0]=i;
		tmp->site[1]=j;
		tmp->site[2]=k;
		tmp->site[3]=l;
		tmp->prev=tmp1;
		tmp1=tmp;
		if (ptmp[(i+1)%L][j][k][l]==0){
			ptmp[(i+1)%L][j][k][l]=1;
			tmp->site[0]=(i+1)%L;
			tmp->var=0;
		}
		else if (ptmp[i][(j+1)%L][k][l]==0){
			ptmp[i][(j+1)%L][k][l]=1;
			tmp->site[1]=(j+1)%L;
			tmp->var=1;
		}
		else if (ptmp[i][j][(k+1)%L][l]==0){
			ptmp[i][j][(k+1)%L][l]=1;
			tmp->site[2]=(k+1)%L;
			tmp->var=2;
		}
		else if (ptmp[i][j][k][(l+1)%L]==0){
			ptmp[i][j][k][(l+1)%L]=1;
			tmp->site[3]=(l+1)%L;
			tmp->var=3;
		}
		else {
			
		};
	};
		
		
	while(tmp1!=NULL){
		i=tmp->site[0];
		j=tmp->site[1];
		k=tmp->site[2];
		l=tmp->site[3];
		tmp1=tmp->prev;
		switch (tmp->var){
			case 0: 
				*(p[i][j][k][l])=*(p[(i+1)%L][j][k][l])+*(p[i][(j+1)%L][k][l]+1)+*(p[i][j][(k+1)%L][l]+2)+*(p[i][j][k][(l+1)%L]+3)-*(p[i][j][k][l]+1)-*(p[i][j][k][l]+2)-*(p[i][j][k][l]+3);
			break;
			
			case 1:
				*(p[i][j][k][l]+1)=*(p[(i+1)%L][j][k][l])+*(p[i][(j+1)%L][k][l]+1)+*(p[i][j][(k+1)%L][l]+2)+*(p[i][j][k][(l+1)%L]+3)-*(p[i][j][k][l])-*(p[i][j][k][l]+2)-*(p[i][j][k][l]+3);
			break;
			
			case 2:
				*(p[i][j][k][l]+2)=*(p[(i+1)%L][j][k][l])+*(p[i][(j+1)%L][k][l]+1)+*(p[i][j][(k+1)%L][l]+2)+*(p[i][j][k][(l+1)%L]+3)-*(p[i][j][k][l])-*(p[i][j][k][l]+1)-*(p[i][j][k][l]+3);
			break;
			
			case 3:
				*(p[i][j][k][l]+3)=*(p[(i+1)%L][j][k][l])+*(p[i][(j+1)%L][k][l]+1)+*(p[i][j][(k+1)%L][l]+2)+*(p[i][j][k][(l+1)%L]+3)-*(p[i][j][k][l])-*(p[i][j][k][l]+1)-*(p[i][j][k][l]+2);
			break;
		};
		free(tmp);
		tmp=tmp1;
				
	};
	free(tmp);
	return 0;
}





double heat(double *p[L][L][L][L], double E, double beta){
	int accept=1;
	unsigned long long i, j, k, l, mi;
	double E_new, M[L][L][L][L][4], *ptmp[L][L][L][L], dA, q, expon, a, phi, chi, y, DE;
	double _Complex A, V, X;
	FILE *itpDE;
	//char title6[100]="proposed_DS_b1.txt";
	//printf("ok1\n");
	//itpDE=fopen(title6, "a+");
	for (i=0; i<L; i++){
		for (j=0; j<L; j++){
			for (k=0; k<L; k++){
				for (l=0; l<L; l++){
					ptmp[i][j][k][l]=&M[i][j][k][l][0];
					for (mi=0; mi<4; mi++){
						*(ptmp[i][j][k][l]+mi)=*(p[i][j][k][l]+mi);
					}
				}
			}
		}
	}
						
	/*i=rand()%L;
	j=rand()%L;
	k=rand()%L;
	l=rand()%L;
	mi=rand()%4;
	*/
	i=((unsigned long long)(floor(gsl_rng_uniform(r)*makeint)))%L;
	j=((unsigned long long)(floor(gsl_rng_uniform(r)*makeint)))%L;
	k=((unsigned long long)(floor(gsl_rng_uniform(r)*makeint)))%L;
	l=((unsigned long long)(floor(gsl_rng_uniform(r)*makeint)))%L;
	mi=((unsigned long long)(floor(gsl_rng_uniform(r)*makeint)))%4;
	
	//printf("i=%d, j=%d, k=%d, l=%d, mi=%d\n", i, j, k, l, mi);
	/*
	A= cexp(I*(*(p[(i+1)%L][j][k][l]+1) - *(p[i][(j+1)%L][k][l]) - *(p[i][j][k][l]+1))) + cexp(I*(-(*(p[(i+1)%L][(j-1+L)%L][k][l]+1)) - (*(p[i][(j-1+L)%L][k][l])) + *(p[i][j][k][l]+1)));
	//printf("okA1\n");
	A += cexp(I*(*(p[(i+1)%L][j][k][l]+2) - *(p[i][j][(k+1)%L][k]) - *(p[i][j][k][l]+2))) + cexp(I*(-(*(p[(i+1)%L][j][(k+L-1)%L][l]+2)) - (*(p[i][j][(k-1+L)%L][l])) + *(p[i][j][k][l]+2)));
	//printf("okA2\n");
	A += cexp(I*(*(p[(i+1)%L][j][k][l]+3) - *(p[i][j][k][(l+1)%L]) - *(p[i][j][k][l]+3))) + cexp(I*(-(*(p[(i+1)%L][j][k][(l-1+L)%L]+3)) - (*(p[i][j][k][(l-1+L)%L])) + *(p[i][j][k][l]+3)));
	//printf("okA3\n");
	A += cexp(I*(*(p[i][(j+1)%L][k][l]+2) - *(p[i][j][(k+1)%L][l]+1) - *(p[i][j][k][l]+2))) + cexp(I*(-(*(p[i][(j+1)%L][(k+L-1)%L][l]+2)) - (*(p[i][j][(k+L-1)%L][l]+1)) + *(p[i][j][k][l]+2)));
	//printf("okA4\n");
	//printf("j+1 modL is %d, l+L-1modL is%d \n", (j+1)%L, (l+L-1)%L); 
	A += cexp(I*(*(p[i][(j+1)%L][k][l]+3) - *(p[i][j][k][(l+L-1)%L]+1) - *(p[i][j][k][l]+3))) + cexp(I*(-(*p[i][(j+1)%L][k][(l+L-1)%L]+1) - (*(p[i][j][k][(l+L-1)%L]+1)) + *(p[i][j][k][l]+3)));
	//printf("okA5\n");
	A += cexp(I*(*(p[i][j][(k+1)%L][l]+3) - *(p[i][j][k][(l+1)%L]+2) - *(p[i][j][k][l]+3))) + cexp(I*(-(*(p[i][j][(k+1)%L][(l+L-1)%L]+3)) - (*(p[i][j][k][(l+L-1)%L]+2)) + *(p[i][j][k][l]+3)));
	*/
	//I want mi>ni
	/*A= cexp(I*(*(p[i][(j+1)%L][k][l]) - *(p[(i+1)%L][j][k][l]+1) - *(p[i][j][k][l]))) + cexp(I*(-(*(p[(i-1+L)%L][(j+1)%L][k][l])) - *(p[(i-1+L)%L][j][k][l]+1) + *(p[i][j][k][l])));
	A += cexp(I*(*(p[i][j][(k+1)%L][l]) - *(p[(i+1)%L][j][k][l]+2) - *(p[i][j][k][l]))) + cexp(I*(-(*(p[(i-1+L)%L][j][(k+1)%L][l])) - *(p[(i-1+L)%L][j][k][l]+2) + *(p[i][j][k][l])));
	A += cexp(I*(*(p[i][j][k][(l+1)%L]) - *(p[(i+1)%L][j][k][l]+3) - *(p[i][j][k][l]))) + cexp(I*(-(*(p[(i-1+L)%L][j][k][(l+1)%L])) - *(p[(i-1+L)%L][j][k][l]+3) + *(p[i][j][k][l])));
	A += cexp(I*(*(p[i][j][(k+1)%L][l]+1) - *(p[i][(j+1)%L][k][l]+2) - *(p[i][j][k][l]+1))) + cexp(I*(-(*(p[i][(j-1+L)%L][(k+1)%L][l]+1)) - *(p[i][(j-1+L)%L][k][l]+2) + *(p[i][j][k][l]+1)));
	A += cexp(I*(*(p[i][j][k][(l+1)%L]+1) - *(p[i][(j+1)%L][k][l]+3) - *(p[i][j][k][l]+1))) + cexp(I*(-(*(p[i][(j-1+L)%L][k][(l+1)%L]+1)) - *(p[i][(j-1+L)%L][k][l]+3) + *(p[i][j][k][l]+1)));
	A += cexp(I*(*(p[i][j][k][(l+1)%L]+2) - *(p[i][j][(k+1)%L][l]+3) - *(p[i][j][k][l]+2))) + cexp(I*(-(*(p[i][j][(k-1+L)%L][(l+1)%L]+2)) - *(p[i][j][(k-1+L)%L][l]+3) + *(p[i][j][k][l]+2)));
	//printf("ok2\n");
	a= sqrt(pow(creal(A),2) + pow(cimag(A), 2));
	V = A/a;
	phi= acos(creal(V));
	*/
	
	while(accept==1){
		if (gsl_rng_uniform(r)>0.5){
			*(ptmp[i][j][k][l]+mi)=gsl_rng_uniform(r)*pi;
		}
		else{
			*(ptmp[i][j][k][l]+mi)=-gsl_rng_uniform(r)*pi;
		}
		//*(ptmp[i][j][k][l]+mi)=*(p[i][j][k][l]+mi) + ((((unsigned long long)(floor(gsl_rng_uniform(r)*makeint))%2)*2)-1)*(gsl_rng_uniform(r))/12;
		y = gsl_rng_uniform(r);
		y *= multconst;
		gauge(ptmp);
		E_new = action(ptmp);
		DE= E_new-E;
		
		//if (E_new==E){
		//	printf("ok dentro if\n");
		//	return heat(p, E, beta);
		//}
		//fprintf(itpDE, "%.12lf\n", DE);
		if (DE<DE_min){
			DE_min=DE;
		}
		if (DE>DE_max){
			DE_max=DE;
		}
		
		if (y < exp(-E_new+E)){
			accept=0;
			*(p[i][j][k][l]+mi)=*(ptmp[i][j][k][l]+mi);
		}
			
	}
	//fclose(itpDE);
	//printf("ok3\n");
	/*if((chi-phi)<(-1*pi)){
		*(p[i][j][k][l]+mi)=chi-phi+2*pi;
	}
	else if((chi-phi)>(1*pi)){
		*(p[i][j][k][l]+mi)=chi-phi-2*pi;
	}
	else{
		*(p[i][j][k][l]+mi)=chi-phi;
	}
	*/
	/*
	for (i=0; i<L; i++){
		for (j=0; j<L; j++){
			for (k=0; k<L; k++){
				for (l=0; l<L; l++){
					ptmp[i][j][k][l]=&M[i][j][k][l][0];
					for (mi=0; mi<4; mi++){
						*(ptmp[i][j][k][l]+mi) = *(p[i][j][k][l]+mi);
					};
				};
			};
		};
	};
	
	dA=((rand()%2)*2 -1)*(double)rand()/(double)RAND_MAX;
	dA /= 2.3;
	*(ptmp[i][j][k][l]+mi) += dA;
	gauge(ptmp);
	
	E_new=action(ptmp);
	
	if (E_new==E){
		return metropolis(p, E, beta);
	};
	expon=exp(E-E_new);
	if (E_new<=E){
		acc++;
		for (i=0; i<L; i++){
			for (j=0; j<L; j++){
				for (k=0; k<L; k++){
					for (l=0; l<L; l++){
						for (mi=0; mi<4; mi++){
							*(p[i][j][k][l]+mi)=*(ptmp[i][j][k][l]+mi);
						};
					};
				};
			};
		};
		return E_new;
	}
	
	else{
		q=(double)rand()/RAND_MAX;
		if (q<exp(E-E_new)){
			acc++;
			for (i=0; i<L; i++){
				for (j=0; j<L; j++){
					for (k=0; k<L; k++){
						for (l=0; l<L; l++){
							for (mi=0; mi<4; mi++){
								*(p[i][j][k][l]+mi)=*(ptmp[i][j][k][l]+mi);
							};
						};
					};
				};
			};
			return E_new;
		};
		return E;
	};
	*/
	gauge(p);
	E=action(p);
	//printf("ok4\n");
	return E;
}



double action(double *p[L][L][L][L]){
	double S=0, f1, f2;
	int i, j, k, l, mi, ni, itmp1, jtmp1, ktmp1, ltmp1, itmp2, jtmp2, ktmp2, ltmp2, a, midual, nidual, itmp3, jtmp3, ktmp3, ltmp3, itmp4, jtmp4, ktmp4, ltmp4, c;
	for (i=0; i<L; i++){
		for (j=0; j<L; j++){ 
			for (k=0; k<L; k++){
				for (l=0; l<L; l++){
					f1=0;
					f2=0;
					//mi>ni
					for (mi=1; mi<4; mi++){
						if (mi==1){
							itmp1=i;
							jtmp1=(j+1)%L;
							ktmp1=k;
							ltmp1=l;
						};
						if (mi==2){
							itmp1=i;
							jtmp1=j;
							ktmp1=(k+1)%L;
							ltmp1=l;
						};
						if (mi==3){
							itmp1=i;
							jtmp1=j;
							ktmp1=k;
							ltmp1=(l+1)%L;
						};
						for (ni=0; ni<mi; ni++){
							if (ni==0){
								itmp2=(i+1)%L;
								jtmp2=j;
								ktmp2=k;
								ltmp2=l;
							};
							if (ni==1){
								itmp2=i;
								jtmp2=(j+1)%L;
								ktmp2=k;
								ltmp2=l;
							};
							if (ni==2){
								itmp2=i;
								jtmp2=j;
								ktmp2=(k+1)%L;
								ltmp2=l;
							};
							
							f1 += pow((*(p[itmp1][jtmp1][ktmp1][ltmp1]+ni) - *(p[i][j][k][l] + ni) + *(p[i][j][k][l] + mi) - *(p[itmp2][jtmp2][ktmp2][ltmp2]+mi)),2);
							
							
							//f1 = f1/4;

// Here I am computing (F_mi_ni)^2=(A_ni(x+mi)-A_ni(x)+A_mi(x)-A_mi(x+ni))^2      where the "vector" tmp1=x+mi and tmp2=x+ni 
							
							midual=-1;
							for (a=0; a<4; a++){
								if (mi != a && ni != a){
									if (midual == -1){
										midual= a;
										if (midual==0){
											itmp3=(i+1)%L;
											jtmp3=j;
											ktmp3=k;
											ltmp3=l;
										};
										if (midual==1){
											itmp3=i;
											jtmp3=(j+1)%L;
											ktmp3=k;
											ltmp3=l;
										};
										if (midual==2){
											itmp3=i;
											jtmp3=j;
											ktmp3=(k+1)%L;
											ltmp3=l;
										};
										if (midual==3){
											itmp3=i;
											jtmp3=j;
											ktmp3=k;
											ltmp3=(l+1)%L;
										};
									}
									else{
										nidual= a;
										if (nidual==0){
											itmp4=(i+1)%L;
											jtmp4=j;
											ktmp4=k;
											ltmp4=l;
										};
										if (nidual==1){
											itmp4=i;
											jtmp4=(j+1)%L;
											ktmp4=k;
											ltmp4=l;
										};
										if (nidual==2){
											itmp4=i;
											jtmp4=j;
											ktmp4=(k+1)%L;
											ltmp4=l;
										};
										if (nidual==3){
											itmp4=i;
											jtmp4=j;
											ktmp4=k;
											ltmp4=(l+1)%L;
										};
									};
								};
							};
							c=-1;
							if (nidual==2 && midual==0){
								c=1;
							}
							if (nidual==3 && midual==1){
								c=1;
							}
							f2 +=  c*(*(p[itmp1][jtmp1][ktmp1][ltmp1]+ni) - *(p[i][j][k][l] + ni) + *(p[i][j][k][l] + mi) - *(p[itmp2][jtmp2][ktmp2][ltmp2]+mi))*(*(p[itmp3][jtmp3][ktmp3][ltmp3]+nidual) - *(p[i][j][k][l] + nidual) + *(p[i][j][k][l] + midual) - *(p[itmp4][jtmp4][ktmp4][ltmp4]+midual));
			//This is F_mi_ni* dual(F_mi_ni)
							
							
							
						};						
					};
					
					f2 = pow(f2, 2)/4;
					//f1 = 2*f1;	
					/*if(i==(L-2)){
						printf("f2=%lf \n", f2);
						printf("f1=%lf \n", f1);
					}*/
					S += (sqrt(1+ f1/beta + f2/(pow(beta, 2))) -1);
					//S += (sqrt(1+f1/beta) -1);
					
					/*if(i==(L-2) && j==4 && k==2 && l==(L-1)){
						printf("S partial before= %lf \n", S);
					}*/
					/*if(i==(L-2)){
						printf("S partial after= %lf \n", S);
					}*/
				};
			};
		};
	};
	return beta*S;
}




//double initializeb(){
		
